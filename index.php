<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Demo Gobusiness</title>
</head>
<body>
    <h1>Demo GoBusiness</h1>

    <div id="form">
        <div>
            <label for="username">Username</label>
            <input type="text" id="username" name="username">
        </div>
        <div>
            <label for="password">Password</label>
            <input type="text" id="password" name="password">
        </div>
        <div>
            <button id="login">Login</button>
        </div>
    </div>
    <p id="message"></p>

    <div>
        <button id="loadUsers">Obtener Usuarios</button>
    </div>

    <script src="https://code.jquery.com/jquery-2.2.3.js" integrity="sha256-laXWtGydpwqJ8JA+X9x2miwmaiKhn8tVmOVEigRNtP4=" crossorigin="anonymous"></script>
    <script src="main.js"></script>
</body>
</html>