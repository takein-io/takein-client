
$(document).ready(function() {

    var lblMessage = $('#message');
    var accessToken = '';

    // Objeto enviado a oauth-server
    var data = {
        'grant_type': 'password',
        'client_id': 'testclient',
        'client_secret': 'secret',
        'scope': 'basic',
        'username': '',
        'password': ''
    }

    $('#login').on('click', function () {
        var username = $('#username').val(),
            password = $('#password').val();

        data.username = username;
        data.password = password;

        $.ajax({
            type: 'post',
            url: 'http://gobusinessapi.dev:8080/api/v1/access_token',
            data: data,
            dataType: 'json',
            statusCode: {
                401: function () {
                    lblMessage.text('Las credenciales son incorrectas.');
                }
            },
            success: function (response) {
                lblMessage.text('Logeado!');
                accessToken = response.access_token;
            },
            fail: function (httpObj, textStatus) {
                lblMessage.text('Error: ' + textStatus);
            }
        });

    });

    $('#loadUsers').on('click', function () {
        $.ajax({
            type: 'get',
            url: 'http://gobusinessapi.dev:8080/api/v1/users/',
            headers: {
                "Authorization" : "Bearer " + accessToken
            },
            dataType: 'json',
            statusCode: {
                401: function () {
                    lblMessage.text('No está logeado para esta acción.');
                }
            },
            success: function (response) {
                lblMessage.text('Usarios listados. Revise el log de consola :)');
                console.log(response);
            },
            fail: function (httpObj, textStatus) {
                lblMessage.text('Error: ' + textStatus);
            }
        });
    });

});